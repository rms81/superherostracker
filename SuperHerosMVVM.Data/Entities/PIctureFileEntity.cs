﻿using System;

namespace SuperHerosMVVM.Data.Entities
{
    public class PictureFileEntity
    {
        public Guid Id { get; set; }
        public string FilePath { get; set; }
    }
}