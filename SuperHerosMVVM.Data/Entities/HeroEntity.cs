﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SuperHerosMVVM.Data.Entities
{
    public class HeroEntity
    {
        public Guid Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string EmailAddress { get; set; }
        public string HomeAddress { get; set; }
        public string WorkAddress { get; set; }
        public string SpecialSuperPower { get; set; }
        public string Company { get; set; }
        public string HeroicWords { get; set; }
        public Guid? PictureId { get; set; }
        public PictureFileEntity PictureFileInfo { get; set; }
        public ICollection<string> KnownEnemies { get; } = new HashSet<string>();
    }
}
