﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows.Documents;
using Caliburn.Micro;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using SuperHerosMVVM.ActiveHero;
using SuperHerosMVVM.Data.Entities;
using SuperHerosMVVM.Models;

namespace SuperHerosMVVM
{
    public class ShellViewModel:Conductor<Screen>, IShell
    {
        private HeroViewModel _activeHero;
        private BindableCollection<HeroViewModel> _heroes=new BindableCollection<HeroViewModel>();

        public HeroViewModel ActiveHero
        {
            get { return _activeHero; }
            set
            {
                if (Equals(value, _activeHero)) return;
                _activeHero = value;
                NotifyOfPropertyChange(() => ActiveHero);
            }
        }

        public BindableCollection<HeroViewModel> Heroes
        {
            get { return _heroes; }
            set
            {
                if (Equals(value, _heroes)) return;
                _heroes = value;
                NotifyOfPropertyChange(() => Heroes);
            }
        }

        public ShellViewModel()
        {
            //_heroes.Add(new HeroViewModel() {FirstName = "First Name", LastName = "Last Name", PictureSourcePath = "../HeroPictures/AntMan.PNG",EmailAddress = "antman@marvel.com",Company = "Marvel"});

            //_heroes.Add(new HeroViewModel() { FirstName = "First Name", LastName = "Last Name", PictureSourcePath = "../HeroPictures/AntMan.PNG" });
        }

        protected override void OnInitialize()
        {
            base.OnInitialize();

            LoadSuperHeroes();
        }

        private void LoadSuperHeroes()
        {
            var pictures = LoadPictures();
            var heroes=new List<HeroEntity>();

            using (StreamReader file = File.OpenText("./DataFiles/Heroes.json"))
            {
                using (JsonTextReader reader=new JsonTextReader(file))
                {
                    var jobject = JToken.ReadFrom(reader).SelectToken("Heroes").ToString();

                    heroes = JsonConvert.DeserializeObject<List<HeroEntity>>(jobject);
                }
            }

            foreach (var heroEntity in heroes)
            {
                var picture = pictures.SingleOrDefault(p => p.Id==heroEntity.PictureId);

                heroEntity.PictureFileInfo = picture;

                _heroes.Add(new HeroViewModel()
                {
                    Id = heroEntity.Id,
                    FirstName = heroEntity.FirstName,
                    LastName = heroEntity.LastName,
                    EmailAddress = heroEntity.EmailAddress,
                    HeroicWords = heroEntity.HeroicWords,
                    Company = heroEntity.Company,
                    SpecialSuperPower = heroEntity.SpecialSuperPower,
                    PictureSourcePath = $"../HeroPictures/{heroEntity.PictureFileInfo?.FilePath}",
                });
            }
        }

        private ICollection<PictureFileEntity> LoadPictures()
        {
            using (StreamReader file = File.OpenText("./DataFiles/PictureFiles.json"))
            {
                using (JsonTextReader reader = new JsonTextReader(file))
                {
                    var jobject = JToken.ReadFrom(reader).SelectToken("Files").ToString();

                    var files = JsonConvert.DeserializeObject<List<PictureFileEntity>>(jobject);

                    return files;
                }
            }
        }
    }
}