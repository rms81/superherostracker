﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SuperHerosMVVM.Models
{
    public class HeroModel
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string EmailAddress { get; set; }
        public string HomeAddress { get; set; }
        public string SpecialSuperPower { get; set; }
        public string Company { get; set; }
        public string HeroicWords { get; set; }
        public string PictureSourcePath { get; set; }
    }
}
