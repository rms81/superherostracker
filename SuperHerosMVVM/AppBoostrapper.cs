﻿using System;
using System.Collections.Generic;
using System.Windows;
using Caliburn.Micro;
using SuperHerosMVVM.ActiveHero;

namespace SuperHerosMVVM
{
    public class AppBoostrapper:BootstrapperBase
    {
        private SimpleContainer _container;

        public AppBoostrapper()
        {
            Initialize();
        }

        protected override void Configure()
        {
            base.Configure();
            SetupContainer();
        }

        private void SetupContainer()
        {
            _container = new SimpleContainer();

            _container.Singleton<IWindowManager, WindowManager>();
            _container.Singleton<IEventAggregator, EventAggregator>();
            _container.PerRequest<IShell, ShellViewModel>();
            _container.PerRequest<HeroViewModel>();
        }
        
        protected override void OnStartup(object sender, StartupEventArgs e)
        {
            DisplayRootViewFor<IShell>();
        }

        protected override object GetInstance(Type serviceType, string key)
        {
            return _container.GetInstance(serviceType, key);
        }

        protected override IEnumerable<object> GetAllInstances(Type serviceType)
        {
            return _container.GetAllInstances(serviceType);
        }

        protected override void BuildUp(object instance)
        {
            _container.BuildUp(instance);
        }

    }
}