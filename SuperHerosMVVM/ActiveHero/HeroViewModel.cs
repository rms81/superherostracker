﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Caliburn.Micro;

namespace SuperHerosMVVM.ActiveHero
{
    public class HeroViewModel: Screen
    {
        private string _firstName;
        private string _lastName;
        private string _emailAddress;
        private string _specialSuperPower;
        private string _company;
        private string _heroicWords;
        private string _pictureSourcePath;
        private bool _canEdit=false;

        public Guid Id { get; set; }

        public string FirstName
        {
            get { return _firstName; }
            set
            {
                if (value == _firstName) return;
                _firstName = value;
                NotifyOfPropertyChange(() => FirstName);
            }
        }

        public string LastName
        {
            get { return _lastName; }
            set
            {
                if (value == _lastName) return;
                _lastName = value;
                NotifyOfPropertyChange(() => LastName);
            }
        }

        public string EmailAddress
        {
            get { return _emailAddress; }
            set
            {
                if (value == _emailAddress) return;
                _emailAddress = value;
                NotifyOfPropertyChange(() => EmailAddress);
            }
        }

        public string SpecialSuperPower
        {
            get { return _specialSuperPower; }
            set
            {
                if (value == _specialSuperPower) return;
                _specialSuperPower = value;
                NotifyOfPropertyChange(() => SpecialSuperPower);
            }
        }

        public string Company
        {
            get { return _company; }
            set
            {
                if (value == _company) return;
                _company = value;
                NotifyOfPropertyChange(() => Company);
            }
        }

        public string HeroicWords
        {
            get { return _heroicWords; }
            set
            {
                if (value == _heroicWords) return;
                _heroicWords = value;
                NotifyOfPropertyChange(() => HeroicWords);
            }
        }

        public string PictureSourcePath
        {
            get { return _pictureSourcePath; }
            set
            {
                if (value == _pictureSourcePath) return;
                _pictureSourcePath = value;
                NotifyOfPropertyChange(() => PictureSourcePath);
            }
        }

        public bool CanEdit
        {
            get { return _canEdit; }
            set
            {
                if (value == _canEdit) return;
                _canEdit = value;
                NotifyOfPropertyChange(() => CanEdit);
                NotifyOfPropertyChange(() => CanSave);
                NotifyOfPropertyChange(() => CanCancel);
            }
        }

        public bool CanSave => _canEdit;

        public bool CanCancel => _canEdit;

        public void EnableEdit()
        {
            CanEdit = true;
        }

        public void Cancel()
        {
            CanEdit = false;
        }

        public void Save()
        {

        }
    }
}
